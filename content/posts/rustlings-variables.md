---
title: "Rustlings solution | variables"
date: 2023-10-03T20:00:00+00:00
tags: ["rust", "rustlings"]
author: "Me"
showToc: true
TocOpen: false
draft: false
hidemeta: false
comments: false
summary: "Solution for rustlings variables exercises"
disableHLJS: true
disableShare: false
disableHLJS: false
hideSummary: false
searchHidden: true
ShowReadingTime: true
ShowBreadCrumbs: true
ShowPostNavLinks: true
ShowWordCount: true
ShowRssButtonInSectionTermList: true
UseHugoToc: true
---

## variables1.rs

```rust
fn main() {
    let x = 5;
    println!("x has the value {}", x);
}
```

## variables2.rs

```rust
fn main() {
    let x: u16 = 32;
    if x == 10 {
        println!("x is ten!");
    } else {
        println!("x is not ten!");
    }
}
```

## variables3.rs

```rust
fn main() {
    let x: i32 = 6;
    println!("Number {}", x);
}
```

## variables4.rs

```rust
fn main() {
    let mut x = 3;
    println!("Number {}", x);
    x = 5; // don't change this line
    println!("Number {}", x);
}

```

## variables5.rs

```rust
fn main() {
    let number = "T-H-R-E-E"; // don't change this line
    println!("Spell a Number : {}", number);
    {
        let number = 3; // don't rename this variable
        println!("Number plus two is : {}", number + 2);
    }
}

```

## variables6.rs

```rust
const NUMBER: u8 = 3;
fn main() {
    println!("Number {}", NUMBER);
}
```
